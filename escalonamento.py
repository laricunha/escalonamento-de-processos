instante1 = {
    'processo': 'A',
    'prioridade': 4,
    'tempo_execucao': 2,
    'tamanho': 4
}

instante2 = {
    'processo': 'B',
    'prioridade': 2,
    'tempo_execucao': 4,
    'tamanho': 2
}

instante3 = {
    'processo': 'C',
    'prioridade': 1,
    'tempo_execucao': 1,
    'tamanho': 3
}

instante4 = {
    'processo': 'D',
    'prioridade': 3,
    'tempo_execucao': 2,
    'tamanho': 5
}

instante5 = {
    'processo': 'E',
    'prioridade': 1,
    'tempo_execucao': 1,
    'tamanho': 5
}

instante6 = {
    'processo': 'F',
    'prioridade': 5,
    'tempo_execucao': 4,
    'tamanho': 6
}

instante7 = {
    'processo': 'G',
    'prioridade': 3,
    'tempo_execucao': 2,
    'tamanho': 2
}

instante8 = {
    'processo': 'H',
    'prioridade': 1,
    'tempo_execucao': 3,
    'tamanho': 1
}

instantes = []
instantes.append(instante1)
instantes.append(instante2)
instantes.append(instante3)
instantes.append(instante4)
instantes.append(instante5)
instantes.append(instante6)
instantes.append(instante7)
instantes.append(instante8)

newInstantes = sorted(instantes, key=lambda k: k['prioridade' or 'processo' or 'tamanho'])

print(newInstantes)

x = newInstantes[0].get('tempo_execucao')

for i in range(newInstantes.__sizeof__()):
    print("O Processo", newInstantes[0].get('processo'), 'está em execução.')
    print('Tempo de execução do processo:', newInstantes[0].get('tempo_execucao'))
    for i in range(newInstantes[0].get('tempo_execucao')):
        i = newInstantes[0].get('tempo_execucao') - i
        y = i - 1
        print('Tempo de execução do processo:', y)
        if y == 0:
            print('Processo', newInstantes[0].get('processo'), 'foi executado')
            if newInstantes != []:
                z = newInstantes.remove(newInstantes[0])

    if newInstantes == []:
        print('Todos os processos foram executados')
    if newInstantes != []:
        print(newInstantes)